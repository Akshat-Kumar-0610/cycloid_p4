#include "/usr/local/share/p4c/p4include/core.p4"
#include <v1model.p4>


const bit<16> TYPE_CYCLOID = 0x1000;

register<bit<3>> (2)tb1;
register<bit<3>> (2)tb2;
register<bit<3>> (2)tb3;
register<bit<3>> (2)tb4;
register<bit<3>> (2)tb5;
register<bit<3>> (2)tb6;
register<bit<3>> (2)tb7;

action init_register() {
    tb1.write(0, 2);
    tb1.write(0, 7);
    tb2.write(0, 2);
    tb2.write(0, 7);
    tb3.write(0, 2);
    tb3.write(0, 7);
    tb4.write(0, 2);
    tb4.write(0, 1);
    tb5.write(0, 1);
    tb5.write(0, 1);
    tb6.write(0, 2);
    tb6.write(0, 0);
    tb7.write(0, 2);
    tb7.write(0, 2);
}
typedef bit<9>  egressSpec_t;
typedef bit<48> macAddr_t;
typedef bit<32> ip4Addr_t;

/* Header for cycloid routing */	

header ethernet_t {
    macAddr_t dstAddr;
    macAddr_t srcAddr;
    bit<16>   etherType;
}

header cycloid_t {
	bit<2> cyclic_idx;
	bit<3> cubic_idx;
	bit<2> dest_cyclic_idx;
	bit<3> dest_cubic_idx;

	bit<2> current_cyclic_idx;
	bit<3> current_cubic_idx;
	bit<2> current_dest_cyclic_idx;
	bit<3> current_dest_cubic_idx;
}

header ipv4_t {
    bit<4>    version;
    bit<4>    ihl;
    bit<8>    diffserv;
    bit<16>   totalLen;
    bit<16>   identification;
    bit<3>    flags;
    bit<13>   fragOffset;
    bit<8>    ttl;
    bit<8>    protocol;
    bit<16>   hdrChecksum;
    ip4Addr_t srcAddr;
    ip4Addr_t dstAddr;
}

struct headers {
    ethernet_t   ethernet;
    cycloid_t   cycloid;
    ipv4_t       ipv4;
}


/********** Metadata **********/
struct metadata {
    /* empty */
}

/* Parser for Cycloid */
parser CycloidParser(packet_in packet,
                out headers hdr,
                inout metadata meta,
                inout standard_metadata_t standard_metadata) {

    state start {
        transition parse_ethernet;
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_CYCLOID: parse_cycloid;
            default: accept;
        }
    }

    state parse_cycloid {
        packet.extract(hdr.cycloid);
        transition accept;
    }

}

/********** checksum Verification **********/
control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
    apply {  }
}

/********** Ingress **********/
control CycloidIngress (inout headers hdr,
                  inout metadata meta,
                  inout standard_metadata_t standard_metadata) {
    action drop() {
        mark_to_drop(standard_metadata);
    }
    
    action ipv4_forward(macAddr_t dstAddr, egressSpec_t port) {
        standard_metadata.egress_spec = port;
        hdr.ethernet.srcAddr = hdr.ethernet.dstAddr;
        hdr.ethernet.dstAddr = dstAddr;
        hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
    }

    table ipv4_lpm {
        key = {
            hdr.ipv4.dstAddr: lpm;
        }
        actions = {
            ipv4_forward;
            drop;
            NoAction;
        }
        size = 1024;
        default_action = drop();
    }


    
    action MSDB (out bit<2> msdb){
    	if (hdr.cycloid.dest_cubic_idx[2:2] == hdr.cycloid.current_dest_cubic_idx[2:2]){
    		msdb = 2;  	
      	}
    	if (hdr.cycloid.dest_cubic_idx[1:1] == hdr.cycloid.current_dest_cubic_idx[1:1]){
    		msdb = 1;
    	}
    	if (hdr.cycloid.dest_cubic_idx[0:0] == hdr.cycloid.current_dest_cubic_idx[0:0]){
    		msdb = 0;
    	}
    }

    action routing(bit<2> msdb) {
      

        bit<3> temp;
        
        if (hdr.cycloid.current_dest_cyclic_idx < msdb) {
            // hdr.cycloid.current_dest_cyclic_idx = hdr.cycloid.dest_cyclic_idx;
            // hdr.cycloid.current_cubic_idx = hdr.cycloid.dest_cubic_idx;
            tb6.read(hdr.cycloid.current_dest_cubic_idx, 1);
            tb6.read(temp, 0);
            hdr.cycloid.current_dest_cyclic_idx= temp[1:0];
        } else if (hdr.cycloid.current_dest_cyclic_idx == msdb) {
            tb1.read(hdr.cycloid.current_dest_cubic_idx, 1);
            tb6.read(temp, 0);
            hdr.cycloid.current_dest_cyclic_idx= temp[1:0];
        } else if (hdr.cycloid.current_dest_cyclic_idx > msdb) {
            tb4.read(hdr.cycloid.current_dest_cubic_idx,1);
            tb6.read(temp, 0);
            hdr.cycloid.current_dest_cyclic_idx= temp[1:0];
        } else {
            tb5.read(hdr.cycloid.current_dest_cubic_idx ,1);
            tb6.read(temp, 0);
            hdr.cycloid.current_dest_cyclic_idx= temp[1:0];
            
        }
    }

    apply {

        init_register();

        if(hdr.cycloid.isValid()){
            bit<2> msdb;
            MSDB(msdb);        
            hdr.cycloid.current_cubic_idx=hdr.cycloid.current_dest_cubic_idx;
            hdr.cycloid.current_cyclic_idx=hdr.cycloid.current_dest_cyclic_idx;
            routing(msdb);
        }

        if (hdr.ipv4.isValid()) {
            ipv4_lpm.apply();
        }
    // apply(msdb);
    }
}
    

/********** Egress **********/
control MyEgress(inout headers hdr,
                 inout metadata meta,
                 inout standard_metadata_t standard_metadata) {
    apply {  }
}

/********** Checksum **********/
control MyComputeChecksum(inout headers  hdr, inout metadata meta) {
     apply {
        update_checksum(
            hdr.ipv4.isValid(),
            { hdr.ipv4.version,
              hdr.ipv4.ihl,
              hdr.ipv4.diffserv,
              hdr.ipv4.totalLen,
              hdr.ipv4.identification,
              hdr.ipv4.flags,
              hdr.ipv4.fragOffset,
              hdr.ipv4.ttl,
              hdr.ipv4.protocol,
              hdr.ipv4.srcAddr,
              hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}

/********** Deparser **********/
control MyDeparser(packet_out packet, in headers hdr) {
    apply {
        packet.emit(hdr.cycloid);
        packet.emit(hdr.ethernet);
        packet.emit(hdr.ipv4);
    }
}

    
V1Switch(
CycloidParser(),
MyVerifyChecksum(),
CycloidIngress(),
MyEgress(),
MyComputeChecksum(),
MyDeparser()
) main;
