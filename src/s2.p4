#include <code.p4>
#include <v1model.p4>

#include "cyc2.p4"

register<bit<5>> (7) tb; = {22, 4, 31, 1, 0, 23, 5};
#include <code.p4>
#include <v1model.p4>

const bit<16> TYPE_CYCLOID = 0x1000;

typedef bit<9>  egressSpec_t;
typedef bit<48> macAddr_t;
typedef bit<32> ip4Addr_t;

/* Header for cycloid routing */	

header ethernet_t {
    macAddr_t dstAddr;
    macAddr_t srcAddr;
    bit<16>   etherType;
}

header cycloid_t {
	bit<2> cyclic_idx;
	bit<3> cubic_idx;
	bit<2> dest_cyclic_idx;
	bit<3> dest_cubic_idx;

	bit<2> current_cyclic_idx;
	bit<3> current_cubic_idx;
	bit<2> current_dest_cyclic_idx;
	bit<3> current_dest_cubic_idx;
}

header ipv4_t {
    bit<4>    version;
    bit<4>    ihl;
    bit<8>    diffserv;
    bit<16>   totalLen;
    bit<16>   identification;
    bit<3>    flags;
    bit<13>   fragOffset;
    bit<8>    ttl;
    bit<8>    protocol;
    bit<16>   hdrChecksum;
    ip4Addr_t srcAddr;
    ip4Addr_t dstAddr;
}

struct headers {
    ethernet_t   ethernet;
    cycloid_t   cycloid;
    ipv4_t       ipv4;
}


/********** Metadata **********/
struct metadata {
    /* empty */
}

/* Parser for Cycloid */
parser CycloidParser(packet_in packet,
                out headers hdr,
                inout metadata meta,
                inout standard_metadata_t standard_metadata) {

    state start {
        transition parse_ethernet;
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_CYCLOID: parse_cycloid;
            default: accept;
        }
    }

    state parse_cycloid {
        packet.extract(hdr.cycloid);
        transition accept;
    }

}

/********** checksum Verification **********/
control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
    apply {  }
}

/********** Ingress **********/
control CycloidIngress (inout headers hdr,
                  inout metadata meta,
                  inout standard_metadata_t standard_metadata) {
    action drop() {
        mark_to_drop(standard_metadata);
    }
    
    action MSDB (out msdb){
    	if (hdr.cycloid.dest_cubic_idx[2:2] == current_dest_cubic_idx[2:2]){
    		msdb = 2;  	
      	}
    	if (hdr.cycloid.dest_cubic_idx[1:1] == current_dest_cubic_idx[1:1]){
    		msdb = 1;
    	}
    	if (hdr.cycloid.dest_cubic_idx[0:0] == current_dest_cubic_idx[0:0]){
    		msdb = 0;
    	}
    }

    action routing(out k) {
        
    	if (hdr.cycloid.currentCyclicIndex < msdb) {
		hdr.cycloid.current_cyclic_index = hdr.cycloid.dest_cyclic_index;
		hdr.cycloid.current_cubic_index = hdr.cycloid.dest_cubic_index;
        hdr.cycloid.current_dest_cubic_idx = tb[4]<< 2 + tb[4] << 4 + tb[4] << 8 + tb[4] << 16 + tb[4] << 32 + tb[4] << 64;
        } else if (hdr.cycloid.currentCyclicIndex == msdb) {
            hdr.cycloid.current_dest_cubic_idx = tb[0]<<2 + tb[0]<<4 + tb[0]<<8 + tb[0]<<16+tb[0]<<32+tb[0]<<64;
        } else if (hdr.cycloid.currentCyclicIndex > msdb) {
            hdr.cycloid.current_dest_cyclic_idx = tb[1]<<2 + tb[1]<<4 + tb[1]<<8 + tb[1]<<16+tb[1]<<32+tb[1]<<64;
        } else {
           cycloid.current_dest_cyclic_idx = tb[1]<<2 + tb[4]<<4 + tb[4]<<8 + tb[4]<<16+tb[4]<<32+tb[4]<<64; 
        }
    }
    apply(msdb);
}
    
    
