

Current node conifg 3D CCC
	d = 3 here
	total node = 24

	<1, 000> <2, 000> ------------ <2, 001> <0,001> \
	     <0, 000>                       <1, 001>	  \ 
	 |   	 \							     |         \  
	 |	   	  \                              |           \
	 |	   	  <0, 011> <1, 011> -------------|- <1, 010>  <0, 010>
	 |  	         <2, 011>		  		 |  	  <2, 010>
 	 |                 |                     | 
                       |                     |            |
	<1, 100> <2, 100> -|----------- <2, 101> <1,101>      |
	     <0, 000>      |                    <0, 101> \    |
	     	 \		   |			        		    \ |
		   	   \       |                                  |\
		 	 <0, 111>  |<1. 111> --------------- <1, 110> | <0, 110>
		         <2, 111>		    	 			  <2, 110>


"leaf_set": [ 
    /* Index 0: Predecessor in the inside leaf set */,
    /* Index 1: Successor in the inside leaf set */, 
    /* Index 2: Primary node in the preceding outside leaf set */,
    /* Index 3: Primary node in the succeeding outside leaf set */
] 